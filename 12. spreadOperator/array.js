let ar1 = [1, 2, 3];
let ar2 = [10, 11];

let ar3 = ["a", "b", ...ar1];
let ar4 = ["a", "b", ...ar1, ...ar2, "nitan"];

console.log(ar3);
console.log(ar4);
