/* if (false) {
  console.log("I am if");
} else {
  console.log("I am else");
} */

/* 
else block will execute if non of the block above it gets executed
*/

/* let age =  20
if(age > 18) {
  console.log("He can join club")
}
else {
  console.log("He cannot join club")
} */

//if number is even console => the number is even else number is odd

let number = 5;
if (number % 2 === 0) {
  console.log("The number is even.");
} else {
  console.log("The number is odd.");
}

/* 
One block will execute from one chain in given example we have 3 if chains thus 3 block will be executed.
*/