/* let gender = "male";

if (gender === "male") {
  console.log("He is male");
} else if (gender === "female") {
  console.log("She is female");
} else if (gender === "other") {
  console.log("Other");
} else {
  console.log("None");
} */

/* 
age  0 to 18   => Underage
age  19 to 60  => Adult
age  61 to 150=> Old
else none
*/

/* let age = 24;

if (age >= 0 && age <= 18) {
  console.log("Underage");
} else if (age >= 19 && age <= 60) {
  console.log("Adult");
} else if (age >= 61 && age <= 150) {
  console.log("Old");
} else {
  console.log("None");
} */

/* 
if age is 25,  console your ticket is free
			if age is 26,  console your ticket cost 100
			if age is 27,  console your ticket cost 200
			if age is other than 25,26,27 console you are not allowed
*/

/* let age = 27;

if (age != 25 && age != 26 && age != 27) {
  console.log("You are not allowed");
} else if (age === 25) {
  console.log("Your ticket is free");
} else if (age === 26) {
  console.log("Your ticket cost 100");
} else if (age === 27) {
  console.log("Your ticket cost 200");
} else {
  console.log("None");
} */

/* 
if age [from 1 to 17],  console your ticket is free
			if age[18 to 25 ],  your ticket cost 100
			else,  your ticket cost 200
*/

let age = 60

if(age>=1 && age<=17) {
  console.log("Your ticket is free")
} else if(age>=18 && age<=25) {
  console.log("Your ticket cost 100")
} else {
  console.log("Your ticket cost 200")
}