console.log("a");

setTimeout(() => {
  console.log("c");
}, 3000);

setTimeout(() => {
  console.log("d");
}, 2000);

setTimeout(() => {
  console.log("e");
}, 1000);

console.log("b");

/* 
Node: Runs JS code
  Node has call stack, memory queue (FIFO), event loop

Call stack: It runs code inside it
  Once it runs code it popped of code

Memory queue: It is a queue for function works on FIFO principle 

Event loop: It is a mediator which continuously monitor call stack and memory queue if call stack is empty it push the memory queue function to the call stack

Asynchronous function: Any function which push tasks to background is called asynchronous function. Eg: setTimeout and setInterval
*/