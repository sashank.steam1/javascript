let a = 1;

let interval2 = setInterval(() => {
  console.log("I am interval 2");
}, 5000);

let interval1 = setInterval(() => {
  console.log(a);
  a = a + 1;
  if (a === 10) {
    clearInterval(interval1);
    clearInterval(interval2);
  }
}, 3000);
