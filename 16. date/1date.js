let date = new Date(); //yyyy-mm-ddThh:mm:ss => iso format
/* 
2024-03-28 valid iso date
2024-3-28 invalid iso date because 3 is one digit
*/
console.log(date);

let date1 = new Date().toLocaleString();
let date2 = new Date().toLocaleDateString();
let date3 = new Date().toLocaleTimeString();

console.log(date1);
console.log(date2);
console.log(date3);
