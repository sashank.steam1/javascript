console.log(true && true && true); //true
console.log(true && false && true); //false

console.log(true || false || true); //true
console.log(false || false || false); //false

console.log(!false); //true
console.log(!true); //false
console.log(!(true || false || true)); //false

/* 
&&
true if all are true

||
true if one is true

!
!false => true
!true => false
*/
