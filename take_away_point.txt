JS is a single threaded synchronous blocking language. 

We can do multi comment by pressing alt+shift+a simultaneously.

Data types:

- number: 1, 2, 3, 4.1
- string: "a", "sashank", "@", "#"
- boolean: true/false

typeof data => It gives the type of a data.

+ operator => It is the only operator which works for number as well as string. 
              If we plus two numbers it will sum two numbers. (1 + 2 = 3)
              If we plus two strings it will concat two strings. ("1" + "2" = "12")
              ("1" + 2 = "12") If there is war between string and number always string operation wins.

Operations are performed between two. 
1 + 1 + "1" + 1 + 1 = "2111" (Interview)
2 + "1" + 1 + 1
"21" + 1 + 1
"211" + 1
"2111"

Rules for variables:
  Variable names must be descriptive.
  We cannot use any symbols except _ and $.
  Use camel case for declaring variables.

Convention:
  fileName => camelcase (js)
  FileName => pascalcase 
  file-name => kebabcase (css) (url)
  FILE_NAME => uppercase (.env)

() => parenthesis
{} => curly braces or block
[] => array

String:
  String can be defined by single quote, double quotes or backtik
  Backtik has special power (we can call variable)

Array:
  Array is used to store multiple value of same or different type.

How to  write path:
  . means its own folder
  ../ means its parent folder
  / is used to say arko
  .../ is not possible