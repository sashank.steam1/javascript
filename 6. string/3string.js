// String methods

console.log("sashank".toUpperCase());
console.log("SASHANK".toLowerCase());

console.log("    sashank    ".trim());
console.log("    hello sashank    ".trim());
console.log("    hello sashank    ".trimStart());
console.log("    hello sashank    ".trimEnd());

console.log("Hello Sashank".includes("ank"));

console.log("Hello Sashank".replace("ell", "a")); //it replace first match
console.log("Hello Sashank".replaceAll("a", "h")); //it replace all match

console.log("Hello Sashank".startsWith("Hell")); //true
console.log("Hello Sashank".startsWith("hell")); //false

console.log("Hello Sashank".endsWith("ank")); //true
console.log("Hello Sashank".startsWith("s")); //false

console.log("Sashank".length); //7
