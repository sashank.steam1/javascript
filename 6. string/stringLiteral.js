let name = "Sashank";
let age = 24;
let address = "Teku";

let details = `My name is ${name}. I am ${age} years old. I live in ${address}`;

console.log(details);

let a = 10;
let b = 12;

/* 
${}:
  is used in ``
  give only one value
  we can perform js operations such as +, -, *, /, variable call, function call
*/

let output = `sashank ${a + 1} ${b + 2}`;
