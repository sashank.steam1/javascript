/* 
export can be done in two ways:
- export by name
- export by default
*/

// export by name
export let name = "Nitan";
export let age = 30;
export let isMarried = false;
export let college = "prime"

/* 
We must export by using export keyword
While importing we must use curly braces
They must have same name while import
In a file we can export many variable by using export by name
*/

// export by default
let address = "gagalphedi"

export default address;

/* 
We must use export default keyword
While importing curly braces is not required
It is not necessary to have same name
In a file we can export only one variable using export by default
*/