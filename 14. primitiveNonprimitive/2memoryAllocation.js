/* let ar1 = [1, 2, 3];
let ar2 = [1, 2, 3];
let ar3 = ar1;
console.log(ar1 === ar2); //false
console.log(ar1 === ar3); //true */

/* Memory allocation in primitive and non primitive */

//primitive

let a = 1;
let b = 1;
let c = a;

// if let word is used memeory space is created

//non primitive

let ar1 = [1, 2];
let ar2 = [1, 2];
let ar3 = ar1;

/* 
in case of non primitive let is not sufficient to create memory
first it see whether the variable is copy of another variable if it is, then it does not create memory instead it shares 
*/