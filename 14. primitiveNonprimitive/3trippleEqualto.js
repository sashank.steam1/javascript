// === in case of primitive and non primitive

//primitive

let a = 1;
let b = 1;
let c = a;

//in case of primitive === sees the value
//true if values are same

console.log(a === b); //true
console.log(a === c); //true

//non primitive

let ar1 = [1, 2];
let ar2 = [1, 2];
let ar3 = ar1;

//in case of non primitive === sees the address(reference)
//true if address is same (or if they share same memory)

console.log(ar1 === ar2); //false
console.log(ar1 === ar3); //true
