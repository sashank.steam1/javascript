/* 
2 types of data:
primitive: string, number, boolean, undefined, null
non primitive: array, object, date, error, set...
*/

/* 
Explain about pass by value and pass by reference
pass by value => primitive
pass by reference => non primitive
*/