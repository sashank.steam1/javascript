console.log(Boolean(""));

/* 
Interview (What are truthy and falsy value)

Number
0 => false
1, 2, ..... => true

String
"" => false
All other => true
"a", "Sashank", "*", " ", "0", "false"
*/