// define array
let names = ["sashank", "nitan", "ram", 2, false];
//               0         1       2

// call elements of array
console.log(names);
console.log(names[0]);
console.log(names[1]);
console.log(names[2]);

// change element of array
names[2] = "shyam";
console.log(names);
