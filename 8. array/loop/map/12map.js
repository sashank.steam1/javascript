let task1 = (input) => {
  let output = input.map((value, i) => {
    if (i === 0) {
      return value.toUpperCase();
    } else {
      return value.toLowerCase();
    }
  });
  return output;
};

console.log(task1(["s", "A", "s", "H", "A", "N", "K"]));
