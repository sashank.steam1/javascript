/* let ar1 = [1, 3, 4, 5];

let ar2 = ar1.map((value, i) => {
  if (value % 2 === 0) {
    return value * 0;
  } else {
    return value * 100;
  }
});

console.log(ar2); */

let task1 = (input) => {
  let output = input.map((value, i) => {
    if (value % 2 === 0) {
      return value * 0;
    } else {
      return value * 100;
    }
  });
  return output;
};

console.log(task1([1, 3, 4, 5]));
