let task1 = (input) => {
  let output = input.map((value, i) => {
    if (i % 2 === 0) {
      return value;
    } else {
      return value * 100;
    }
  });
  return output;
};

console.log(task1([1, 2, 3, 4]));