/* 
Task 1: "saSHank" => "Sashank"
*/

/* let task1 = (input) => {
  let ar1 = input.split("");
  let ar2 = ar1.map((value, i) => {
    if (i === 0) {
      return value.toUpperCase();
    } else {
      return value.toLowerCase();
    }
  });
  return ar2.join("");
};

console.log(task1("saSHank")); */

/* 
Task 2: ["my","name","is"] => ["My","Name","Is"]
*/

/* let task2 = (input) => {
  let output = input.map((value, i) => {
    let ar1 = value.split("");
    let ar2 = ar1.map((value, i) => {
      if(i === 0) {
        return value.toUpperCase()
      } else {
        return value.toLowerCase()
      }
    })
    return ar2.join("");
  });
  return output;
};

console.log(task2(["my", "name", "is"])); */

/* 
Task 3: "my name is sashank" => "My Name Is Sashank"
*/

let task3 = (input) => {
  let ar1 = input.split(" ");
  let output = ar1.map((value, i) => {
    let ar2 = value.split("");
    let ar3 = ar2.map((value, i) => {
      if (i === 0) {
        return value.toUpperCase();
      } else {
        return value.toLowerCase();
      }
    });
    return ar3.join("")
  });
  return output.join(" ");
};

console.log(task3("my name is sashank"));
