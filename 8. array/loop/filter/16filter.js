/* ["a",1,"b",3,"sashank"] = ["a","b","sashank"] */

let ar1 = ["a", 1, "b", 3, "sashank"];

let ar2 = ar1.filter((value, i) => {
  if (typeof(value) === 'string') {
    return true;
  }
});

console.log(ar2);
