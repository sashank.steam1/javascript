// [1,2,3,99,10] => [99, 10] =>filter value from 10 to 100

let ar1 = [1, 2, 3, 99, 10];

let ar2 = ar1.filter((value, i) => {
  if (value >= 10 && value <= 100) {
    return true;
  }
});

console.log(ar2);
