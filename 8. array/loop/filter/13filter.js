/*  
filter():
  - it is used to filter input element
  - output is in array
  - output elements are the elements from the input
  - it must return true(pass value) or false(block value)
  - by default filter will return false
*/

let ar1 = [20, 9, 10, 15];

let ar2 = ar1.filter((value, i) => {
  if (value > 12) {
    return true;
  } else {
    return false;
  }
});

console.log(ar2);
