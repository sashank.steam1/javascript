//output will be true if every input returns true

let ar1 = [20, 30, 40, 50];

let v = ar1.every((value, i) => {
  if (value > 30) {
    return true;
  }
});

console.log(v);
