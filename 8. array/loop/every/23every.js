/* 
check whether we have all even number in the list [2,4,9,6]
*/

let ar1 = [2, 4, 8, 6];

let hasEven = ar1.every((value, i) => {
  if (value % 2 === 0) {
    return true;
  }
});

console.log(hasEven);
