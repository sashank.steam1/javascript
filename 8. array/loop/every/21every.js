/* 
check whether we have all ages greater than 17 form the given input [1,2,20,30,40]
*/

let ages = [1, 2, 20, 30, 40];

let check = ages.every(
  (value,
  (i) => {
    if (value > 17) {
      return true;
    }
  })
);

console.log(check);
