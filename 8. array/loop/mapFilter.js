/* 
map:
  - input(array) and output(array) length are same
  - it is used to modify element

filter:
  - input(array) and output(array) no need to have same length
  - it is used to filter element
  - output element must be from input element
*/