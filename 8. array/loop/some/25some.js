/* 
using some method find whether we have Bearer in the the string "Bearer token"
*/

let string = "Bearer token";

let arr = string.split(" ");

let hasBearer = arr.some((value, i) => {
  if (value === "Bearer") {
    return true;
  }
});

console.log(hasBearer);
