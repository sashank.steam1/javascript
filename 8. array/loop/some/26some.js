/* 
check whether a person has smoking habit   ["smoking", "drinking", "biting nails"]
*/

let habits = ["smoking", "drinking", "biting nails"];

let hasSmoking = habits.some((value, i) => {
  if (value === "smoking") {
    return true;
  }
});

console.log(hasSmoking);
