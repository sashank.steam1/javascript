/* 
check  whether we have nitan in the list ["utshab","nitan","ram","hari"]
*/

let guests = ["utshab", "nitan", "ram", "hari"];

let hasNitan = guests.some((value, i) => {
  if (value === "nitan") {
    return true;
  }
});

console.log(hasNitan);
