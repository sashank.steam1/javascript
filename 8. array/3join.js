let arr = ["My", "name", "is"];

let str1 = arr.join(" ");
let str2 = arr.join("*");
let str3 = arr.join("$");

console.log(str1);
console.log(str2);
console.log(str3);

//It converts array to string
