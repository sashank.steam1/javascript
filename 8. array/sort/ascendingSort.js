let ar1 = ["a", "c", "b"];

/* 
["a", "c", "b"] = ["a", "b", "c"]
[9, 3] = [3, 9]
[9, 10] = [10, 9] ***** (interview question)
["ad", "ac"] = ["ac", "ad"] 
["a", "z", "A", "Z"] = ["A", "Z", "a", "z"] (uppercase then lowercase)
*/

let _ar1 = ar1.sort();
console.log(_ar1);
