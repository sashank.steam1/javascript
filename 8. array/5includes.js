/* let ar1 = ["nitan", "ram", "kapil"];

let ar2 = ar1.includes("ni");
let ar3 = ar1.includes("nitan");
console.log(ar2);//false
console.log(ar3);//true */

// "Bearer hello" has Bearer in string

/* let str = "Bearer hello";

let arr = str.split(" ");
console.log(arr.includes("Bearer")); */

let hasBearer = (str) => {
  return str.split(" ").includes("Bearer");
};

console.log(hasBearer("My name is Bearer"));
