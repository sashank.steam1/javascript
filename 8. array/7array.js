/* 
ar1.map()
ar1.filter()
ar1.some()
ar1.every()
ar1.reduce()
ar1.includes()
ar1.at()
ar1.length

ar1.push()
ar1.pop()
ar1.unshift()
ar1.shift()

ar1.reverse()
ar1.sort()
*/

/* 
All methods returns some value but 
push, pop, unshift, shift => changes original array
reverse, sort => it changes original array and return new array
*/
