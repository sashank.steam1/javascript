/* 
c = create (add) //age, address
r = read (get) //all, address
u = update
d = delete
*/

let info = { name: "Sashank" };

//create
info.age = 24;
info.address = "Teku";

//read
console.log(info);
console.log(info.address);

//update
info.name = "Ram";

//delete
delete info.age;

console.log(info);
