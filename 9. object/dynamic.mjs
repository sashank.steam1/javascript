// dynamic properties on object

/* 
  - We can call variable inside value directly
  - But to call variable in key we must use[]
*/

let value1 = "nitan";
let value2 = 30;

let key1 = "name";
let key2 = "age";

let info = {
  [key1]: value1,
  [key2]: value2,
};

console.log(info);
console.log(info.name);
console.log(info.age);
