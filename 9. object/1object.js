/* 
Object is used to store multiple values.
Array is the collection of value.
Object is the collection of key value pair(property).
key + value = property
*/

let info = {
  age: 30,
  isMarried: false,
  name: "Nitan",
};

//get whole object
console.log(info);

//get specific value
console.log(info.age);
console.log(info.isMarried);
console.log(info.name);

//delete specific property
delete info.isMarried;

//change specific value
info.name = "Ram";
