/* 
sum(1, 2, 3, 4) // return 10
*/

let sum = (...num) => {
  let res = num.reduce((pre, cur) => {
    return pre + cur;
  }, 0);
  return res;
};

console.log(sum(1, 2, 3, 4, 10));
